#!/bin/bash

aws cloudformation describe-stacks --stack-name setupClusterWithRegistry

if [[ $? -ne 0 ]]; then
    echo "Stack doesn't exist so create new stack"
    aws cloudformation create-stack --stack-name setupClusterWithRegistry --template-body file://CloudFormationTemplates/cluster.yaml --capabilities CAPABILITY_IAM --capabilities CAPABILITY_NAMED_IAM
else
    echo "Stack already exists so update existing stack"
    aws cloudformation create-stack --stack-name setupClusterWithRegistry --template-body file://CloudFormationTemplates/cluster.yaml --capabilities CAPABILITY_IAM --capabilities CAPABILITY_NAMED_IAM