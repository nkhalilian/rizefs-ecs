# RizeFS-ECS

Deploy sinatra and webrick on a cluster of EC2s in two regions. The container orchestration service is ECS.

This project consists of two microservices that are deployed to a cluster of EC2 compute units. Used technologies are:
- GitLab code and image repository
- Gitlab deployment pipeline
- Docker to containerize the microservices
- ECS to manage the containers on cluster of Amazon EC2 instances
- CloudFormation to provision and configure the cluster
- CloudFormation to deploy the containers
- Elastic load balancer to route the trrafic 

## Environment
The project structure is as follow:

- build-docker-img

<pre> 
Contain the code for the both microservices (sinatra and webrick), Dockerfiles, puma ruby file to run sinatra on puma web server instead of the default webrick. </pre> 

- release-pipeline
<pre> 
CloudFormationTemplates -> contains the CloudFormation templates to bootstrap the infrastructure. 
    cluster.yaml provisions the networking components, the ECS cluster and the ECR registry. 
    services.yaml includes the task definition, services and loadbalancer components (SG, listener and target groups)
scripts -> contains the scripts that are invoked in the gitlab pipeline to build the code, push the image, provision the cluster and deploy to the cluster

</pre> 




## TODO
Add a logic for directing the trrafic to different ports based on a defined schedule. The ports 1212 and 206 are exposed on both of the containers (sinatra and webrick), however, currently load balancer only sends the load to fixed ports sinatra:1212 and webrick:206. 



